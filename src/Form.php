<?php
namespace uhi67\form;

use Exception;
use DOMElement;
use ReflectionException;
use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\BaseModel;
use uhi67\uxapp\Component;
use uhi67\uxapp\Model;
use uhi67\uxapp\Module;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxapp\UXAppPage;
use uhi67\uxml\UXMLElement;

/**
 * # Html form widget #
 *
 * The html form default posts itself up to the same action
 *
 * ## Using ##
 *
 * ### In the controller action ###
 *
 * ```
 *	$form = new Form(['page'=> $this, 'name'=>'domnew', 'model'=>$model]);
 *	if($form->export($model) && $model->save()) {
 *		$this->redirect($back_url);
 *	}
 *	$form->createNode($this, $formoptions, true, $modeloptions);	// Form frame with model data included
 * ```
 * where:
 * - `$formoptions` array of options for each attributes, see {@see Form::createAttributeNode()}
 * - `$modeloptions`: see {@see Form::createAttributeNode()}
 *
 * ### In the xml view ###
 * ```xhtml
 * <!-- automatic display with defaults -->
 * <xsl:apply-templates select="form" />
 *
 * <!-- you may override single field template -->
 * <xsl:template match="attribute[@id='your-field-id']" mode="row">
 * 	<xsl:call-template name="attribute-row" /> <!-- recall the default -->
 * 	<xsl:variable name="model" select="../*[name()=current()/../@modelnode]" />
 * 	<tr>
 * 		<th>Label2</th>
 * 		<td><i class="fa fa-info-circle info"></i><i>Additional row with any content to value <xsl:value-of select="$model/@otherfield" /></i></td>
 * 	</tr>
 * </xsl:template>
 *
 * <!-- you may override the whole default template -->
 * <xsl:template match="form[@name='domnew']" mode="content">
 * 	<input type="hidden" name="id" value="{dom/@id}"/>
 * 	<table class="grid data">
 * 		<tr>
 *			<xsl:variable name="attribute" select="attribute[@name='name']" />
 * 			<td><xsl:value-of select="$attribute/@label" /></td>
 * 			<td>
 * 				<input type="text" name="name" alt="{$attribute/@title}" class="{$attribute/@rules}" size="30" maxlenght="64" value="{@name}" />
 * 			</td>
 * 		</tr>
 * 		<tr>
 *			<td>Tulajdonos egység:</td>
 * 			<td>
 * 				<input type="hidden" id="field_owner" name="owner" value="{@owner}" />
 * 				<input type="text" id="field_owner_name" name="owner_name" size="30" maxlenght="64" disabled="1" onChange="0;" title="Tulajdonos egység"
 *					class="select-org" value="{@owner_name}" />
 * 			</td>
 * 		</tr>
 * 	</table>
 * </xsl:template>
 * ```
 *
 * ### `$config` members
 *
 * - Module **$page** -- the container page (mandatory)
 * - string **$name** -- the name of form instance. The id of the form tag will be form_name. Default is model classname.
 * - {@see BaseModel} **$model** -- model instance for this form instance
 * - string **$class** -- custom class names for form tag
 * - string **$attributes** -- custom attributes for form element in XML (as in {@see createNode()})
 * - array **$notused** -- names of not used fields - the will not be generated and loaded
 * - array **$fields** -- names of used fields - default is all fields except of declared in notused
 * - boolean **$edit** -- true if form is editable, false if readonly (default is true)
 * - {@see DOMElement} **$node_model** -- the node of the model (default is created from model data under form node)
 * - boolean **$showDescr** -- show descriptions default (default is true)
 * - boolean **$showComment** -- show comments default (default is true)
 * - boolean **$showHelp** -- show helps default (default is true)
 *
 * @see Model::refNames() for default of refname
 * @author Peter Uherkovich
 * @copyright 2017-2018
 *
 * @property-read  boolean $posted
 * This form was posted by user, and form variables are in `$_POST['form_formname_variablename']` variables
 * Returns true if the posted variable _form contains the form identifier as value
 * @property-read  string $submit
 * Read only. Returns the javascript action which submits this form.
 * @property-read int $id
 */
class Form extends Module {
    /** var string $_id unique form instance identifier to check post, based on form name, user id and session id */
    private $_id;

    /** @var string $name -- the name of form. The id of the form tag will be form_name. This is the array name posted from html form. */
    public $name;
    /** @var BaseModel|Model $model -- model instance for this form instance */
    public $model;
    /** @var string $class -- custom class names for form tag */
    public $class;
    /** @var array $attributes -- custom attributes for form element in XML see {@see createNode()} */
    public $attributes = [];
    /** @var array $notused -- names of not used fields - the will not be generated and loaded */
    public $notused = [];
    /** @var array $fields -- names of used fields - default is all fields execept of declared in notused */
    public $fields = [];
    /** @var boolean $edit -- true if form is editable, false if readonly */
    public $edit = true;
    /** @var boolean $showDescr -- show descriptions */
    public $showDescr = true;
    /** @var boolean $showDescr -- show comments */
    public $showComment = true;
    /** @var boolean $showDescr -- show helps */
    public $showHelp = true;

    /** @var UXMLElement $node_model -- the node of the model. The model node must be a sibling of the form node. */
    public $node_model;

    /**
     * initializes Form
     *
     * See {@see Form} -- example of usage
     * {@inheritdoc}
     *
     * (empty form, with page property only is allowed to register assets)
     *
     * @param UXAppPage $page
     * @param array $config
     *
     * @throws ReflectionException
     * @throws UXAppException
     */
    function prepare() {
        parent::prepare();
        if($this->notused === null) $this->notused = [];
        if($this->fields === null) $this->fields = [];
        if(!$this->name && $this->model) {
            if(method_exists($this->model, 'tableName')) $this->name = $this->model->tableName();
            else $this->name = Util::uncamelize($this->model->shortName, '-');
        }
        $user_id = $this->page && $this->page->user ? $this->page->user->id : '';
        $this->_id = md5('form_'.$this->name.'_'.$user_id.'_'.session_id());
        if(!$this->fields && $this->model) $this->fields = array_diff($this->model->fields(),  $this->notused);
        $this->page->view->addXslCall('form');
    }

    /** @noinspection PhpUnused */
    function action() {}

    /**
     * ## Creates form node in XML document with attribute subnodes
     *
     * - Adds XML attributes based on `$attributes` property
     * - Attribute nodes are based upon associated model instance.
     * - 'modelnode' attribute should refer to model data.
     * - Optionally creates model node under form node; may be different from associated one.
     * In this case, 'modelnode' generated to point this node.
     *
     * - `$formoptions` is an associative array providing options for each attributes, see {@see Form::createAttributeNode()}
     *      - 0-index provides options for _ALL_ attributes (not only listed here).
     * - `$modeloptions`: see {@see Model::createNode()}
     *
     * ### attributes
     * - comment-class -- classname for field-comment span
     * - descr-class -- classname for field-description rows
     * - help-class -- classname for field-help buttons
     * - table-class
     * - order -- xsl ordering of fields
     * - columns -- for extra columns. Minimum and default is 2
     *
     * @param UXMLElement|Component $node_parent or widget or page
     * @param array $formOptions -- associative array containing options for all attribute nodes, (e.g 'type') {@see Form::createAttributeNode()}
     * @param BaseModel|Model $model -- model data to export to XML (optional) true = use associated model. false= do not generate model node (already exists)
     * @param array $modelOptions -- for model data (e.g 'associations') {@see Model::createNode()}
     *
     * @return UXMLElement the created form node in the DOM
     * @throws ReflectionException
     * @throws UXAppException
     * @throws Exception
     */
    function createNode($node_parent, $formOptions=null, $model=null, $modelOptions=null) {
        $node_parent = $this->parentNode($node_parent);
        if($model===true) $model = $this->model;
        $this->attributes['columns'] = max(2, (int)ArrayUtils::getValue($this->attributes, 'columns', 2));
        $this->node = $node_parent->addNode('form', array_merge($this->attributes, [
            'name' => $this->name,
            'id' => $this->_id,
            'class' => $this->class,
            'model' => get_class($this->model),
            'edit' => (int)$this->edit,
        ]));
        $labels = $this->model->attributeLabels();
        #$hints = $this->model->attributeHints();

        $allOptions = ArrayUtils::getValue($formOptions, 0, []);
        if(!is_array($allOptions)) throw new Exception('$formoptions[0] must be an array');
        /** @var string $attribute -- name of attribute to display as field */
        foreach($this->fields as $attribute) {
            $order = array_search($attribute, array_keys($labels));

            // Default options for the attribute
            $options = array_merge([
                'order' => $order,
                'showDescr' => $this->showDescr,
                'showComment' => $this->showComment,
                'showHelp' => $this->showHelp,
            ], $allOptions);
            // User-defined options will overwrite defaults
            // TODO: default field types from model
            $fieldOptions = ArrayUtils::getValue($formOptions, $attribute, []);
            if(!is_array($fieldOptions)) throw new UXAppException("Field options must be an array ($attribute)");
            $options = array_merge($options, $fieldOptions);
            $this->createAttributeNode($attribute, $options);
        }

        if($model) {
            if(!$this->node_model) {
                $this->node_model = $model->createNode($this->node->parentNode, $modelOptions??[]);
                $this->node->setAttribute('modelnode', $this->node_model->nodeName);
            }
        }
        elseif($this->node_model) {
            $this->node->setAttribute('modelnode', $this->node_model->nodeName);
        }

        return $this->node;
    }

    /**
     * ## Creates an attribute node under form's already prepared node
     *
     * ### Members of `$options`:
     *
     *  - string **label** (default is taken from the model)
     *  - int **order** (default is taken from the model labbel order)
     *  - array|string **hints** (default is taken from the model)
     *  - string **type** (see below)
     *  - Model **refmodel** -- overrides reference model name for select_refmodel class.
     *  - string **refname** -- overrides default name field for reference model
     *  - array **set** -- value set for select fields ('id'=>value, 'name'=>label)
     *  - int **size, maxlen** -- input field size data
     *  - string **class** -- space separated classnames to add to input field's class
     *  - bool **readonly** -- true, if field is displayed in its't declared type, but in read-only mode.
     *  - array **rules** -- extra validating rules
     *  - bool **showDescr** -- show field description
     *  - bool **showHelp** -- show help button
     *  - bool **showComment** -- show field comment
     *  - string **rowclass** -- space separated classnames to add to tr's class
     *  - string **rowgroupclass** -- space separated classnames to add to class of attribute's body part
     *  - string **link** -- show link under value when field or form is read-only
     *  - *any other* attributes to add to XML attribute node
     *
     * If you want a field to be selectable by SelectFrom, you shold not be set type to `'selectFrom'`,
     * register SelectFrom module, and that will render these fields.
     *
     * Similarly, you may define your custom module with an xsl cathcing it's own type-name.
     *
     * For select-type fields:
     * - set option 'set' for immediatly specified set of selectable options
     * - set option 'refset' as name of set elements in content/sets/ node
     * - set option 'refdescr' to true if you want to include the descr attributes from the referenced set.
     *
     * ### Field types
     *
     * #### Non-editable types
     *
     * - hidden: field is not visible, not validated, but will be sent
     * - skipped: field does not appear at all
     * - disabled: field appears readonly, not validated and sent.
     * - readonly: field appears readonly, but validated and sent
     * - view: not an input, only display
     * - link: value is (appended or inserted into link attribute) the link url.
     *
     * #### Editable types if form is editable
     *
     * - text: default
     * - textarea
     * - checkbox, boolean: supports hidden field for sending unchecked value (0). Checked is 1.
     * - radio: set of radio buttons, uses `refset` and `refdescr` attributes
     * - select: uses `refset` and `refdescr` attributes
     * - date: datepicker is displayed if not readonly
     * - ref: fieldname_name is displayed, value is hidden and sent. Editable only through SelectFrom module. Uses `namevalue`, `ref`, `refname` attributes. For ref fields, you should associate `name` fields of referenced tables in `modeloptions`
     * - file: file selector, value will be the uploaded file
     *
     * #### Custom field types
     *
     * Custom field types may be processed by modules
     *
     * - selectFrom: SelectFrom module renders it, similar to standard 'ref'
     *
     *
     * @param string $attribute
     * @param array $options -- associative array with attribute field display options
     *
     * @return DOMElement
     * @throws ReflectionException|UXAppException
     * @throws Exception
     */
    function createAttributeNode($attribute, $options) {
        if($this->notused && in_array($attribute, $this->notused)) return null;

        $modelhints = $this->model->attributeHint($attribute);
        if(!$modelhints) $modelhints = [];
        else if(!is_array($modelhints)) $modelhints = ['hint'=>$modelhints];

        $node_attribute = $this->node->addNode( 'attribute', [
            'id' => strtolower($this->model->shortName).'-'.$attribute,
            'name' => $attribute,
            'type' => $this->model->attributeType($attribute),
            'label' => $this->model->attributeLabel($attribute),
            'order'=>999
        ]);

        $showDescr = ArrayUtils::getValue($options, 'showDescr', true);
        $showComment = ArrayUtils::getValue($options, 'showComment', true);
        $showHelp = ArrayUtils::getValue($options, 'showHelp', true);

        $hints = ArrayUtils::getValue($options, 'hints'); unset($options['hints']);
        if(!$hints) $hints = [];
        else if(!is_array($hints)) $hints = ['hint'=>$hints];

        $hints = array_merge($modelhints, $hints);
        foreach($hints as $ht=>$hv) {
            if($ht == 'descr' &&  !$showDescr) continue;
            if($ht == 'comment' &&  !$showComment) continue;
            if($ht == 'help' &&  !$showHelp) continue;

            if(!is_array($hv)) $hv = [$hv];
            if(is_numeric($ht)) $ht = 'hint';
            foreach($hv as $hl) $node_attribute->addNode( $ht, null, $hl);
        }

        if($this->model instanceof Model) {
            /** @var string $ref -- a foreign key name, like 'parent1' */
            $ref = $this->model->getReference($attribute);
            if($ref) {
                $node_attribute->setAttribute('ref', $ref);
                $fk = $this->model->foreignKey($ref);
                if($fk && $fk[0]??false) {
                    $node_attribute->setAttribute('refmodel', $fk[0]);
                    $reftype = $fk[0];
                    if(is_a($reftype, Component::class, true)) $reftype = strtolower(Util::shortName($reftype));
                    $node_attribute->setAttribute('reftype', $reftype);
                    $node_attribute->setAttribute('refname', $this->model->getRefName($ref));
                }
            }
        }
        // Adding references (if model is a database model)

        /** @var array $rules */
        $rules = $this->model->rules();
        if(isset($rules[$attribute])) {
            foreach($rules[$attribute] as $rule) {
                if(!is_array($rule)) $rule = [$rule];
                $rulename = array_shift($rule);
                Util::camelize($rulename);
                $node_rule = $node_attribute->addNode( 'rule', ['name' => $rulename]);

                /* Insert translated rulemessage for client if no user defined message (only within UXApp classes) */
                $rulemessage = call_user_func([get_class($this->model), 'ruleMessage'], $rulename);
                if($rulemessage && !isset($rule['message'])) $rule['message'] = UXApp::la('uxapp', $rulemessage);

                /** Parameters with numeric keys goes thru json-encoded data subnode to field's data-rulename-data */
                $data = [];
                foreach($rule as $key=>$value) {
                    if(is_int($key)) $data[] = $value;
                    /** @var DOMElement $node_key -- Parameters with string keys goes thru key-named subnodes to field's data-rulename-keyname */
                    else $node_rule->addNode( $key, null, $value);
                }
                if($data) $node_rule->addNode( 'data', null, json_encode($data));
            }
        }

        // Adding form option rules
        $rules = ArrayUtils::getValue($options, 'rules', []);
        foreach($rules as $rule) {
            if(!is_array($rule)) $rule = [$rule];
            $rulename = array_shift($rule);
            Util::camelize($rulename);
            $node_attribute->addNode( 'rule', ['name' => $rulename],  $rule ? json_encode($rule) : null);
        }

        // Adding errors
        $errors = ArrayUtils::getValue($this->model->errors, $attribute, []);
        foreach($errors as $k => $err) {
            if(is_int($k)) $node_attribute->addNode( 'error', null, $err);
        }

        // Add user options. Only not null values will override defaults.
        if(is_array($options)) {
            foreach($options as $name=>$value) {
                if(is_array($value)) {
                    foreach($value as $k=>$v)
                        if(is_array($v)) $node_attribute->addNode( $name, $v);
                        else $node_attribute->addNode( $name, ['id'=>$k], $v);
                }
                else if($value!==null) $node_attribute->setAttribute($name, $value);
            }
        }
        return $node_attribute;
    }

    /** @noinspection PhpUnused */

    /**
     * Determines if this form is posted
     * Checks posted array name and _form identifier.
     *
     * @return boolean
     */
    function getPosted() {
        $formdata = $this->page->app->request->getArray($this->name);
        if(!$formdata) return false;
        $xid = ArrayUtils::getValue($formdata, '_form');
        return $xid == $this->_id;
    }

    /** @noinspection PhpUnused */
    /**
     * Returns the javascript command which submits this form
     * @return string
     */
    function getSubmit() {
        $formid = $this->name;
        return "$('form#form_$formid').submit();";
    }

    /** @noinspection PhpUnused */

    /**
     * Loads form data into the model
     * If validation failed, `Model::$errors` contains errors.
     * Default ($mustvalidate=true) validate only posted fields, or ($mustvalidate=array) given set of fields.
     * To disable validation, specify $mustvalidate=false. To validate all fields, specify $mustvalidate=null.
     *
     * @param BaseModel $model (default is the model instance associated to form)
     * @param bool|array|null $mustvalidate -- true: validate posted fields, false: don't validate, null=validate all fields, array=validate specified fields
     *
     * @return boolean -- success (the posted data was present, and validation passed)
     * @throws UXAppException
     */
    public function export($model=null, $mustvalidate=true) {
        if(!$model) $model = $this->model;
        if($this->posted) {
            $formdata = $this->page->app->request->getArray($this->name);
            if(is_array($files = $this->page->app->request->getFile($this->name))) {
                $formdata = array_merge($formdata, $files);
            }

            // TODO: this assignment may fail on php type validation BEFORE model validation
            $model->attributes = $formdata;
            if($mustvalidate===true) $mustvalidate = array_keys($formdata);
            if($mustvalidate!==false) {
                return $model->validate($mustvalidate);
            }
            else return true;
        }
        UXApp::$app->debug->trace('post not found: $id', ['params'=>['id'=>$this->_id], 'color'=>'#c00']);
        return false;
    }

    /** @noinspection PhpUnused */
    public function getId() {
        return $this->_id;
    }
}
