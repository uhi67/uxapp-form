Form module for UXApp
=====================

Version 1.1 -- 2022-06-22

Installation
------------
Add to your composer.json:
```
{
  "require": {
    "uhi67/uxapp-form": "*"
  },
}
```

Usage
-----

### In the controller:
```
	$form = new Form(['page'=>$page, 'name'=>'domnew', 'model'=>$dom]);
	if($form->export($dom) && $dom->save()) {
		$this->>redirect($back_url);
	}
	$form->createNode($this->node_content, $formoptions, true, $modeloptions);	// Form frame with model data included
```
where:
- `$formoptions` array of options for each attribute, see {@see Form::createAttributeNode()}
- `$modeloptions`: see [Form::createAttributeNode()]()

### In the xml view ###
```xhtml
<!-- automatic display with defaults -->
<xsl:apply-templates select="form" />

<!-- you may override single field template -->
<xsl:template match="attribute[@id='your-field-id']" mode="row">
	<xsl:call-template name="attribute-row" /> <!-- recall the default -->
	<xsl:variable name="model" select="../*[name()=current()/../@modelnode]" />
	<tr>
		<th>Label2</th>
		<td><i class="fa fa-info-circle info"/><i>Additional row with any content to value <xsl:value-of select="$model/@otherfield" /></i></td>
	</tr>
</xsl:template>

<!-- you may override the whole default template -->
<xsl:template match="form[@name='domnew']" mode="content">
	<input type="hidden" name="id" value="{dom/@id}"/>
	<table class="grid data">
		<tr>
			<xsl:variable name="attribute" select="attribute[@name='name']" />
			<td><xsl:value-of select="$attribute/@label" /></td>
			<td>
				<input type="text" name="name" alt="{$attribute/@title}" class="{$attribute/@rules}" size="30" maxlength="64" value="{@name}" />
			</td>
		</tr>
		<tr>
			<td>Tulajdonos egység:</td>
			<td>
				<input type="hidden" id="field_owner" name="owner" value="{@owner}" />
				<input type="text" id="field_owner_name" name="owner_name" size="30" maxlength="64" disabled="disabled" onChange="0;" title="Tulajdonos egység"
					class="select-org" value="{@owner_name}" />
			</td>
		</tr>
	</table>
</xsl:template>
```

Change log
==========

## 1.1 -- 2022-06-22

- default form name is tablename or short classname
- createNode: parent can be any Component
- client side translations (hu)
- client side validation of rules in input class (mandatory, rule-*)
- 'modelnode' attribute added to the form node
- menu placement (moved to inside of <form>)
- some minor fixes

## 1.0 -- 2020-11-03

- first public release
